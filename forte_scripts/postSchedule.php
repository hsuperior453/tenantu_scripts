<?php
    /*
    REST Developer Documentation:       https://www.forte.net/devdocs/api_resources/forte_api_v3.htm
    Best Practices for Payment Forms:   https://www.forte.net/devdocs/reference/payment_forms.htm
    Transaction Response Codes:         https://www.forte.net/devdocs/reference/response_codes.htm
    Frequently Asked Questions:         https://www.forte.net/devdocs/reference/faq.htm
    Forte Technical Support:
                7:00 am - 7:00 pm CST
                866.290.5400 option 5
                integration@forte.net

    ///////////////////////////////////////////////////////////// */

    $base_url          = 'https://sandbox.forte.net/api/v3';     //production: http://api.forte.net/v3
    // Landlord Required Fields
    $organization_id   = 'org_337680';
    $location_id       = 'loc_193969';
    $api_access_id     = 'f82af85197d7f582789271f6ad15a958';
    $api_secure_key    = 'f96cdf737b22ffe2397713ffd03d5d6d';
    $customer_token    = 'cst_nsZ-uMiUKUS8ynCwFsfsLg';
    // END Landlord Required fields
    $auth_token        = base64_encode($api_access_id . ':' . $api_secure_key);
    $schedule_endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $customer_token .'/schedules';

    //Address Info
    $address = array(
        'first_name' => 'Chris',
        'last_name' => 'Carr'
    );

    // Schedule Summary array
    $schedule_summary = array(
    	'schedule_next_date' => ,
    );
    /*$schedule_frequency = array(

    	);
*/
    $schedule_params = array(
    	'customer_token' => ,
    	'paymethod_token' => ,
    	'action'			=> 'sale',
    	'schedule_quantity' => ,
    	'schedule_frequency' => ,
    	'schedule_amount' => ,
    	'schedule_start_date' => ,
    	'schedule_created_date' => ,
    	''
    	);
    //eCheck Info
    $echeck = array(
        'account_holder' => 'Bill G Customer',
        'routing_number' => '091000019',
        'account_number' => '12345678901234',
        'account_type' => 'checking'
    );

    //Credit Card Info
    $card = array(
        'card_type' => 'VISA',
        'name_on_card' => 'Bill G Customer',
        'account_number' => '4111111111111111',
        'expire_month' => '12',
        'expire_year' => '2020',
        'card_verification_value' => '123'
    );

    $params = array(
        'action' => 'sale',        //sale, authorize, credit, void, capture, inquiry, verify, force, reverse
        'echeck' => $echeck,
        'billing_address' => $address,
        'authorization_amount' => 400.00,
        'customer_token' => '',
        'paymethod_token' => '',
        'transaction_id' => '',
        'authorization_code' => ''
    );

    $ch = curl_init($endpoint);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));     //Disable this line for GET's and DELETE's
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic ' . $auth_token,
        'X-Forte-Auth-Organization-id: ' . $organization_id,
        'Accept:application/json',
        'Content-type: application/json'
    ));

    $response = curl_exec($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);
    $data = json_decode($response);

    echo '<pre>';
    print_r('HttpStatusCode: ' . $info['http_code'] . '<br><br>');
    print_r($data);
    echo '\n';
    print_r($response);
    echo '</pre>';
?>
