<?php
    /*
    REST Developer Documentation:       https://www.forte.net/devdocs/api_resources/forte_api_v3.htm
    Best Practices for Payment Forms:   https://www.forte.net/devdocs/reference/payment_forms.htm
    Transaction Response Codes:         https://www.forte.net/devdocs/reference/response_codes.htm
    Frequently Asked Questions:         https://www.forte.net/devdocs/reference/faq.htm
    Forte Technical Support:
                7:00 am - 7:00 pm CST
                866.290.5400 option 5
                integration@forte.net

    ///////////////////////////////////////////////////////////// */


    if (PHP_SAPI === 'cli')
    {
        $get_token = $argv[1];
    }
    else
    {
        $get_token = $_GET['arg1'];
    }
    $base_url          = 'https://sandbox.forte.net/api/v3';     //production: https://api.forte.net/v3
    $organization_id   = 'org_337680';
    $location_id       = 'loc_193969';
    $api_access_id     = 'f82af85197d7f582789271f6ad15a958';
    $api_secure_key    = 'f96cdf737b22ffe2397713ffd03d5d6d';
    $auth_token        = base64_encode($api_access_id . ':' . $api_secure_key);
    //$endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $get_token . '/transactions/';
    $get_trans_endpoint = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/transactions/' . $get_token;

 //   $endpoint          = $base_url . '/organizations/' . $organization_id . '/customers';


    $ch = curl_init($get_trans_endpoint);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'get');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic ' . $auth_token,
        'X-Forte-Auth-Organization-id: ' . $organization_id,
        'Accept:application/json',
        'Content-type: application/json'
    ));

    $response = curl_exec($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);
    $data = json_decode($response);

    echo '<pre>';
    print_r('HttpStatusCode: ' . $info['http_code'] . '<br><br>');
    print_r($data);
    echo '</pre>';
?>
