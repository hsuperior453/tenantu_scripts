#!/bin/bash

########################
## Set up for Laravel ##
########################

function lamp_setup {

	sudo apt-get update
	printf "Installing apache2...\n"
	sudo apt-get install apache2
	
	printf "Installing mysql...\n"
	sudo apt-get install mysql-server php5-mysql
	sudo mysql_install_db
	sudo mysql_secure_installation

	printf "Installing PHP5...\n"
	sudo apt-get install php5 libapache2-mod-php5 php5-mcrypt

	printf "Making info.php...\n"
	sudo cat > /var/www/html/info.php << EOF
	<?php
	phpinfo();
	?>
EOF

	printf "Made info.php inside /var/www/html"



}

printf "Do you have LAMP set up?\n"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) break;;
		No ) lamp_setup; break;;
	esac
done

# Mod Rewrite needs to enable
printf "Running a2enmod rewrite...\n"
sudo a2enmod rewrite
printf "Done\n"

# APC and APCu need to enable
printf "Getting php-apc\n"
sudo apt-get install php-apc
printf "Done\n"

# Need to install Curl
printf "Installing PHP5-Curl...\n"
sudo apt-get install php5-curl
printf "Done\n"

# Need to install imageick
printf "Installing imagick...\n"
sudo apt-get install php5-imagick
sudo php5enmod imagick
printf "Done\n"

# Restart machine
printf "You need to reboot for some changes to take place, reboot now?\n"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) sudo reboot;;
		No ) break;;
	esac
done
