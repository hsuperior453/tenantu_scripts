<?php

    $base_url          = 'https://sandbox.forte.net/api/v3';     //production: http://api.forte.net/v3
    // Landlord Required Fields
    $organization_id   = 'org_337680';
    $location_id       = 'loc_193969';
    $api_access_id     = 'f82af85197d7f582789271f6ad15a958';
    $api_secure_key    = 'f96cdf737b22ffe2397713ffd03d5d6d';
    // END Landlord Required fields


    $email = 'test123@test.com';

    // These Get Name methods are in Controller.php, not TenantsController
//  $tenantFirstName = $this->getTenantFirstName($loggedTenant);
    $tenantFirstName = 'Test';
//  $tenantLastName = $this->getTenantLastName($loggedTenant);
    $tenantLastName = 'One';

    // Address variables
    $street_line1 = '123 Victory Road';
    $street_line2 = '';
    $locality = 'DE';
    $postal_code = '12345';
    $pay_method_label = 'Bank Label';
    $pay_method_notes = 'This bank is my bank';
    
    $auth_token        = base64_encode($api_access_id . ':' . $api_secure_key);
    $endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/';

    //Address/Name Info
    $name = array(
        'first_name' => $tenantFirstName,
        'last_name' => $tenantLastName
    );

    $full_name = $name['first_name'] . ' ' . $name['last_name'];
    
    // Physical address
    $physical_address = array(
        'street_line1' => $street_line1,
        'street_line2' => $street_line2,
        'locality' => $locality,
        'postal_code' => $postal_code
    );
    
    // Address object
    $billingaddress = array(
        'first_name' => $name['first_name'],
        'last_name' => $name['last_name'],
        'label' => $full_name . "'s " . 'Billing Address',
        'email' => $email,
        'address_type' => 'default_billing',
        'shipping_address_type' => 'residential',
        'physical_address' => $physical_address  
    );
    
    
    //eCheck Info to store into Customer PayMethod - Account which rent will be paid from - DONE
    $echeck = array(
        'account_holder' => 'Bill G Customer',
        'routing_number' => '091000019',
        'account_number' => '12345678901234',
        'account_type' => 'checking'
    );
    
    // ARRAY TO STORE PAYment METHOD INTO CUSTOMER PARAMETERS - DONE
    $default_pay_method = array(
        'label' => $pay_method_label,
        'notes' => $pay_method_notes,
        'echeck' => $echeck
    );

    //Credit Card Info - NOT USED
   /* $card = array(
        'card_type' => 'VISA',
        'name_on_card' => 'Bill G Customer',
        'account_number' => '4111111111111111',
        'expire_month' => '12',
        'expire_year' => '2020',
        'card_verification_value' => '123'
    );
*/
    // PARAMETERS OF THE CUSTOMER TO BE MADE
    $params = array(
        'first_name' => $name['first_name'],
        'last_name' => $name['last_name'],
        'paymethod' => $default_pay_method,
        'addresses' => array($billingaddress)
    );

    $ch = curl_init($endpoint);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));     //Disable this line for GET's and DELETE's
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic ' . $auth_token,
        'X-Forte-Auth-Organization-id: ' . $organization_id,
        'Accept:application/json',
        'Content-type: application/json'
    ));

    $response = curl_exec($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);
    $data = json_decode($response);

    echo '<pre>';
    echo '<br><br>';
    echo '<br>';
    echo '<br>';

    echo 'Tenant Pay Method Token: ';
    print_r($data->default_paymethod_token);
    echo '<br>';
    echo 'Tenant Customer Token: ';
    print_r($data->customer_token . '<br>');
    echo 'Line skipped';
    echo '<br>';
    echo 'Tenant Billing Address token: ';
    print_r($data->default_billing_address_token);
    echo '<br>';
    print_r('HttpStatusCode: ' . $info['http_code'] . '<br><br>');
    print_r($data);
    echo '\n';


?>
