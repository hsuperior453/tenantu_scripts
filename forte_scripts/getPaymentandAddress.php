<?php
    /*
    REST Developer Documentation:       https://www.forte.net/devdocs/api_resources/forte_api_v3.htm
    Best Practices for Payment Forms:   https://www.forte.net/devdocs/reference/payment_forms.htm
    Transaction Response Codes:         https://www.forte.net/devdocs/reference/response_codes.htm
    Frequently Asked Questions:         https://www.forte.net/devdocs/reference/faq.htm
    Forte Technical Support:
                7:00 am - 7:00 pm CST
                866.290.5400 option 5
                integration@forte.net

    ///////////////////////////////////////////////////////////// */

    $base_url          = 'https://sandbox.forte.net/api/v3';     //production: https://api.forte.net/v3
    $organization_id   = 'org_337680';
    $location_id       = 'loc_193969';
    $api_access_id     = 'f82af85197d7f582789271f6ad15a958';
    $api_secure_key    = 'f96cdf737b22ffe2397713ffd03d5d6d';
    $auth_token        = base64_encode($api_access_id . ':' . $api_secure_key);
    $pay_endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/paymethods/' . 'mth_n0ZV0U_QvEC3LP8J6gU-EA';
    $addr_endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . 'cst_0aFvebA-p0e53x8uS2g' .'/addresses/' . 'add_tQVB1xj3JECP-M4iljQxoQ';

    function forteGet($endpoint, $auth_token) {
        $ch = curl_init($endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic ' . $auth_token,
        'X-Forte-Auth-Organization-id: ' . /*$organization_id*/'org_337680',
        'Accept:application/json',
        'Content-type: application/json'
    ));
        return $ch;
    }

    $ch = forteGet($pay_endpoint, $auth_token);
   /* curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic ' . $auth_token,
        'X-Forte-Auth-Organization-id: ' . $organization_id,
        'Accept:application/json',
        'Content-type: application/json'
    ));
    */
    $ch1 = forteGet($addr_endpoint, $auth_token);
    /*curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch1, CURLOPT_VERBOSE, 1);
    curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch1, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch1, CURLOPT_CUSTOMREQUEST, 'GET');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
    curl_setopt($ch1, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic ' . $auth_token,
        'X-Forte-Auth-Organization-id: ' . $organization_id,
        'Accept:application/json',
        'Content-type: application/json'
    ));*/



    $response = curl_exec($ch);
    $response1 = curl_exec($ch1);
    $info = curl_getinfo($ch);
    $info = curl_getinfo($ch1);
    curl_close($ch);
    curl_close($ch1);
    $data = json_decode($response);
    $data1 = json_decode($response1);

    $pay_settings_array = json_encode(array(
        'account_holder'    => $data->echeck->account_holder,
        'bank_name'         => $data->label,
        'account_number'    => $data->echeck->masked_account_number,
        'routing_number'    => $data->echeck->routing_number,
        'account_type'      => $data->echeck->account_type,
        'street_line1'      => $data1->physical_address->street_line1,
        'street_line2'      => $data1->physical_address->street_line2,
        'city'              => $data1->physical_address->locality,
        'zip_code'          => $data1->physical_address->postal_code 
    ));   
    
    $tenant_pay_settings = json_decode($pay_settings_array);
    $get_http_success = (($info['http_code'] != 200) ? 0:1);
    $addr_output = array(
        'get_http_success'  => $get_http_success,
        'data'              => $data
    );
    echo '<pre>';

    var_dump($addr_output);
    echo '<br><br>';
    print_r('HttpStatusCode: ' . $info['http_code'] . '<br><br>');
    print_r($data);
    echo '<br><br>';
    print_r($tenant_pay_settings);
    echo '<br>Single Values of pay settings:<br>';
    print_r($tenant_pay_settings->routing_number);
    echo '<br><br><br>';
    print_r($data->echeck->account_holder);
    echo '<br><br>';
    print_r('HttpStatusCode_AddressID: ' . $info['http_code'] . '<br><br>');
    print_r($data1);
    echo '</pre>';
?>
