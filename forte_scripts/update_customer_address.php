<?php
    /*
    REST Developer Documentation:       https://www.forte.net/devdocs/api_resources/forte_api_v3.htm
    Best Practices for Payment Forms:   https://www.forte.net/devdocs/reference/payment_forms.htm
    Transaction Response Codes:         https://www.forte.net/devdocs/reference/response_codes.htm
    Frequently Asked Questions:         https://www.forte.net/devdocs/reference/faq.htm
    Forte Technical Support:
                7:00 am - 7:00 pm CST
                866.290.5400 option 5
                integration@forte.net

    ///////////////////////////////////////////////////////////// */

    $base_url          = 'https://sandbox.forte.net/api/v3';     //production: http://api.forte.net/v3
    // Landlord Required Fields
    $organization_id   = 'org_337680';
    $location_id       = 'loc_193969';
    $api_access_id     = 'f82af85197d7f582789271f6ad15a958';
    $api_secure_key    = 'f96cdf737b22ffe2397713ffd03d5d6d';
    // END Landlord Required fields
    $auth_token        = base64_encode($api_access_id . ':' . $api_secure_key);
    $addr_endpoint     = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/cst_DIGfDLz1M0C-mD5K_quKSw/addresses/add_uuxITxc9-Uqr3vPuFbovfA';
    
    $pay_endpoint      = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/paymethods/' . 'mth_6pZUApPn-0OQY2F1u5zI3w';

    $st_line1 = '*12 Yale Drive';
    $st_line2 = '11D';
    

    //Address Info
    $address = array(
        'first_name' => 'Chris',
        'last_name' => 'Carr'
    );

    //eCheck Info
    $echeck = array(
        'account_holder' => 'Bill G Customer',
        'routing_number' => '091000019',
        'account_number' => '12345678901234',
        'account_type' => 'checking'
    );

    //Credit Card Info
    $card = array(
        'card_type' => 'VISA',
        'name_on_card' => 'Bill G Customer',
        'account_number' => '4111111111111111',
        'expire_month' => '12',
        'expire_year' => '2020',
        'card_verification_value' => '123'
    );
    
    if(strpos($st_line1, '*') != false)
    {
        $physical_address = array(
            'street_line1' => $st_line1,
            'street_line2' => $st_line2,
            'locality' => 'Newark',
            'region' => 'DE',
            'postal_code' => '19711'
        );
    }
    else
    {
        $physical_address = array(
            'street_line2' => $st_line2,
            'locality' => 'Newark',
            'region' => 'DE',
            'postal_code' => '19711'
        );
    }
    $params = array(
        'physical_address' => $physical_address
    );

    $ch = curl_init($addr_endpoint);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));     //Disable this line for GET's and DELETE's
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic ' . $auth_token,
        'X-Forte-Auth-Organization-id: ' . $organization_id,
        'Accept:application/json',
        'Content-type: application/json'
    ));

    $response = curl_exec($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);
    $data = json_decode($response);

    echo '<pre>';
    print_r('HttpStatusCode: ' . $info['http_code'] . '<br><br>');
    print_r($data);
    echo '\n';
    print_r($response);
    echo '</pre>';
?>
