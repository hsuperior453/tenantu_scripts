<?php
    /*
    REST Developer Documentation:       https://www.forte.net/devdocs/api_resources/forte_api_v3.htm
    Best Practices for Payment Forms:   https://www.forte.net/devdocs/reference/payment_forms.htm
    Transaction Response Codes:         https://www.forte.net/devdocs/reference/response_codes.htm
    Frequently Asked Questions:         https://www.forte.net/devdocs/reference/faq.htm
    Forte Technical Support:
                7:00 am - 7:00 pm CST
                866.290.5400 option 5
                integration@forte.net

    ///////////////////////////////////////////////////////////// */

    $base_url          = 'https://sandbox.forte.net/api/v3';     //production: http://api.forte.net/v3
    // Landlord Required Fields
    $organization_id   = 'org_337680';
    $location_id       = 'loc_193969';
    $api_access_id     = 'f82af85197d7f582789271f6ad15a958';
    $api_secure_key    = 'f96cdf737b22ffe2397713ffd03d5d6d';
    $customer_token    = 'cst_nsZ-uMiUKUS8ynCwFsfsLg';
    $paymethod_token   = 'mth_Hm3DznMcpUWU8hWOJhE-Hg';
    // END Landlord Required fields
    $auth_token        = base64_encode($api_access_id . ':' . $api_secure_key);
    $schedule_endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $customer_token .'/schedules';

    $tenant_last_name = 'Su';
    $property_unit_info = 'SL-11D';
    $schedule_created_date = date('m/d/Y h:i:s a', time());

    // Pull from landlord database to see what day they plan to collect rent
    // 0 = Monthly, 1 = Bi-monthly, 2 = Quarterly, 3 = Semi-Annually

    $landlord_rent_freq = 3;

    // Array of schedule frequencies - These should be pulling from the landlord forte payment tables
    $schedule_frequency_array = array('monthly', 'bi_monthly', 'quarterly', 'semi_annually');


    $schedule_amount = 900.00;
    $pay_day_of_month = 2;
    $schedule_start_date = '1/'. $pay_day_of_month .'/17';
    // Quantity will be for a year, so 12 months of rent payment
    $schedule_quantity = 12;
    $test_freq = 'semi-annually';



    // If landlord selects to collect rent on a quarterly basis...
    /*
    if($landlord_rent_freq == 3 || $landlord_rent_freq == 1)
    {
    	$schedule_frequency = preg_quote($schedule_frequency_array[$landlord_rent_freq], '-');
    }
    else
    {
    	$schedule_frequency = $schedule_frequency_array[$landlord_rent_freq];
    }
    */

    // Schedule Summary array
   /* $schedule_summary = array(
    	'schedule_next_date' => ,
    ); */
    /*$schedule_frequency = array(

    	);
*/
    $schedule_params = array(
    	//'customer_token' => $customer_token,
    	'paymethod_token' => $paymethod_token,
    	'action'			=> 'sale',
    	'schedule_quantity' => $schedule_quantity,
    	'schedule_frequency' => $schedule_frequency[1],
    	'schedule_amount' => $schedule_amount,
    	'schedule_start_date' => $schedule_start_date,
    	'schedule_created_date' => $schedule_created_date,
    	'reference_id' => 'RENT-' . $property_unit_info . '-' . $tenant_last_name,
        'item_description' => 'Rent for ' . $property_unit_info
    	);
    
    $ch = curl_init($schedule_endpoint);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($schedule_params));     //Disable this line for GET's and DELETE's
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic ' . $auth_token,
        'X-Forte-Auth-Organization-id: ' . $organization_id,
        'Accept:application/json',
        'Content-type: application/json'
    ));

    $response = curl_exec($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);
    $data = json_decode($response);

    echo '<pre>';
    echo $schedule_frequency[3];
    echo '   ' . $test_freq;
    echo ($schedule_frequency[3] === $test_freq);
    echo '<br>';
    echo gettype($schedule_frequency[3]);
    echo '<br>';
    print_r('HttpStatusCode: ' . $info['http_code'] . '<br><br>');
    print_r($data);
    echo '</pre>';
?>
