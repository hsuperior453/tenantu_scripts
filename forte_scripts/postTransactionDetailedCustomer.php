<?php
    /*
    REST Developer Documentation:       https://www.forte.net/devdocs/api_resources/forte_api_v3.htm
    Best Practices for Payment Forms:   https://www.forte.net/devdocs/reference/payment_forms.htm
    Transaction Response Codes:         https://www.forte.net/devdocs/reference/response_codes.htm
    Frequently Asked Questions:         https://www.forte.net/devdocs/reference/faq.htm
    Forte Technical Support:
                7:00 am - 7:00 pm CST
                866.290.5400 option 5
                integration@forte.net

    ///////////////////////////////////////////////////////////// */

    $base_url          = 'https://sandbox.forte.net/api/v3';     //production: http://api.forte.net/v3
    // Landlord Required Fields
    $organization_id   = 'org_337680';
    $location_id       = 'loc_193969';
    $api_access_id     = 'f82af85197d7f582789271f6ad15a958';
    $api_secure_key    = 'f96cdf737b22ffe2397713ffd03d5d6d';
    // END Landlord Required fields
    $auth_token        = base64_encode($api_access_id . ':' . $api_secure_key);
    $rent_transaction_endpoint = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/transactions';

    $customer_token = 'cst_0aFvebA-p0e53x8uS2IN6g';
    $paymethod_token = 'mth_n0ZV0U_QvEC3LP8J6gU-EA';
    $addr_token = 'add_tQVB1xj3JECP-M4iljQxoQ';

    function forteGet($endpoint, $auth_token) 
    {
        $ch = curl_init($endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic ' . $auth_token,
        'X-Forte-Auth-Organization-id: ' . /*$organization_id*/'org_337680',
        'Accept:application/json',
        'Content-type: application/json'
    	));
        return $ch;
    }

	function fortePost($endpoint, $params, $auth_token) 
	{
	    //$organization_id = Config::get('constants.FORTE_TENANTU_ORG_ID');
        $organization_id   = 'org_337680';
	    $ch = curl_init($endpoint);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_VERBOSE, 1);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
	    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));     //Disable this line for GET's and DELETE's
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	        'Authorization: Basic ' . $auth_token,
	        'X-Forte-Auth-Organization-id: ' . $organization_id,
	        'Accept:application/json',
	        'Content-type: application/json'
	    ));
	    
	    return $ch;
	}

    $addr_endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $customer_token . '/addresses/' . $addr_token;
    
    $tenant_billing_address = forteGet($addr_endpoint, $auth_token);
    $addr_response = curl_exec($tenant_billing_address);
    $addr_resp_info = curl_getinfo($tenant_billing_address);
    curl_close($tenant_billing_address);
    $addr_data = json_decode($addr_response, true);
    
    $unit_group = 'School Lane Garden Apartments';
    $unit_group_abbrv = 'SLGA';
    $property_name = '11D Yale Drive';
    $rent_price = 400.00;
    $service_fee_amount = 1.25;

    //Address Info
    //$get_todays_date = date();
    $get_this_month = date('M');
    $get_todays_day_of_month = date('j');
    //$get_todays_day_and_month = date('j');
    $reference_id = $get_this_month . ' Rent ' . $property_name;

    $rent_transaction_params = array(
        'action' => 'sale',        //sale, authorize, credit, void, capture, inquiry, verify, force, reverse
        'customer_token' => $customer_token,
        //'paymethod_token' => $paymethod_token,
        'billing_address' => $addr_data,
        'authorization_amount' => $rent_price,
        'reference_id' 	=> $reference_id
        //'service_fee_amount' => $service_fee_amount,
    );

    $ch = fortePost($rent_transaction_endpoint, $rent_transaction_params, $auth_token);

   /* $ch = curl_init($rent_transaction_endpoint);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($rent_transaction_params));     //Disable this line for GET's and DELETE's
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic ' . $auth_token,
        'X-Forte-Auth-Organization-id: ' . $organization_id,
        'Accept:application/json',
        'Content-type: application/json'
    ));*/

    $response = curl_exec($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);
    $data = json_decode($response);
    $pay_response = $data->response->response_desc;

    echo '<pre>';
    print_r('HttpStatusCode: ' . $info['http_code'] . '<br><br>');
    print_r($pay_response);
    echo '<br>';
    print_r($data);
    echo '<br>';
    print_r($addr_data);
    echo '<br>';
    echo $get_this_month;
    echo '<br>';
    //echo '<br>' . $get_todays_date;
    //print_r($addr_data);
    echo '</pre>';
?>
